# tintin

My experiments with [tintin++](https://tintin.mudhalla.net/) beta versions.

I'm compiling tintin++ for Windows (in cygwin):

[ Directory1 ](http://files.mud.kharkov.org/tintin/)

[ Directory2 ](http://mud.kharkov.org/files/tintin/)

Prool

----

How to make
-----------------------------

cp tt/src

./configure

make

... in cygwin (Windows)
-----------------------

Need manually in file main.c replace AUTO to UTF-8 in CHARSET line(in init_tintin() function)

proolix@gmail.com
[prool.virtustan.net](http://prool.virtustan.net)

Serhii "Prool" Pustovoitov,
Duisburg, Germany

